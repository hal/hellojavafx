package hellojavafx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class AppController {

    private List<String> currencies = List.of("ECU", "NOK", "USD");
    private Map<String, Double> exchangeRates = new HashMap<>();

    public AppController() {
        exchangeRates.put("ECU-NOK", 10.0d);
        exchangeRates.put("USD-NOK", 5.0d);
        exchangeRates.put("ECU-USD", 2.0d);
    }

    private double convert(String fromCurrency, double fromValue, String toCurrency) {
        if (fromCurrency.equals(toCurrency)) {
            return fromValue;
        }
        String key = fromCurrency + "-" + toCurrency;
        if (exchangeRates.containsKey(key)) {
            double exchangeRate = exchangeRates.get(key);
            return fromValue * exchangeRate;
        }
        key = toCurrency + "-" + fromCurrency;
        if (exchangeRates.containsKey(key)) {
            double exchangeRate = exchangeRates.get(key);
            return fromValue / exchangeRate;
        }
        return fromValue;
    }

    @FXML
    private void initialize() {
        fromCurrencySelector.getItems().addAll(currencies);
        fromCurrencySelector.getSelectionModel().select(0);
        toCurrencySelector.getItems().addAll(currencies);
        toCurrencySelector.getSelectionModel().select(0);
        convertFromValue();
        // listens to changes to the selectedItem property of the selection model of the choice box
        // could have use the onAction event, too
        fromCurrencySelector.getSelectionModel().selectedItemProperty().addListener((prop, oldValue, newValue) -> convertToValue());
        toCurrencySelector.getSelectionModel().selectedItemProperty().addListener((prop, oldValue, newValue) -> convertFromValue());
        // listens to changes to the text property og the text field, i.e. every edit
        fromValueText.textProperty().addListener((prop, oldValue, newValue) -> convertFromValue());
        toValueText.textProperty().addListener((prop, oldValue, newValue) -> convertToValue());
    }

    @FXML
    private ChoiceBox<String> fromCurrencySelector;

    @FXML
    private ChoiceBox<String> toCurrencySelector;
    
    @FXML
    private TextField fromValueText;

    @FXML
    private TextField toValueText;

    private void convertFromValue() {
        String fromCurrency = this.fromCurrencySelector.getSelectionModel().getSelectedItem();
        String toCurrency = this.toCurrencySelector.getSelectionModel().getSelectedItem();
        double fromValue = Double.valueOf(fromValueText.getText());
        double toValue = convert(fromCurrency, fromValue, toCurrency);
        toValueText.setText(Double.toString(toValue));
    }

    private void convertToValue() {
        String fromCurrency = this.fromCurrencySelector.getSelectionModel().getSelectedItem();
        String toCurrency = this.toCurrencySelector.getSelectionModel().getSelectedItem();
        double toValue = Double.valueOf(toValueText.getText());
        double fromValue = convert(toCurrency, toValue, fromCurrency);
        fromValueText.setText(Double.toString(fromValue));
    }
}
